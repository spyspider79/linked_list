
#include<bits/stdc++.h>
#include<ostream>
using namespace std;

class node{
public:
    int data;
    node *next;
    node(int d){
        data=d;
        next=NULL;
    }

};

void insert(node*&head,int data){
    if(head==NULL){
        head=new node(data);
        return;
    }
    node*tail=head;
    while(tail->next!=NULL){
        tail=tail->next;
    }
    tail->next=new node(data);
    return ;
}
//passing by value
void print(node*head){
    while(head!=NULL){
        cout<<head->data<<" ";
        head=head->next;
    }
    cout<<endl;
}
node* kReverse(node*head,int k){
    node*cur=head;
    node*n=NULL;
    node*pre=NULL;
    int cnt=0;
     while(cnt<k&&cur!=NULL){
      n=cur->next;
      cur->next=pre;
      pre=cur;
      cur=n;
      cnt++;
  }
    if(n!=NULL){
        head->next=kReverse(n,k);
    }

  return pre;
}
int main(){
    node*head=NULL;
    int n,k;
    cin>>n>>k;
    for(int i=0;i<n;i++){
        int data;
        cin>>data;
        insert(head,data);
    }
    node*head1=kReverse(head,k);
  //  print(head);
    print(head1);

    return 0;
}
