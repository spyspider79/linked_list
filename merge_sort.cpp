#include<bits/stdc++.h>
#include<ostream>
using namespace std;

class node{
public:
    int data;
    node *next;
    node(int d){
        data=d;
        next=NULL;
    }
};
void print(node*head){
    while(head!=NULL){
        cout<<head->data<<" ";
        head=head->next;
    }
    cout<<endl;
}
void insert(node*&head,int data){
    if(head==NULL){
        head=new node(data);
        return;
    }
    node*tail=head;
    while(tail->next!=NULL){
        tail=tail->next;
    }
    tail->next=new node(data);
    return ;
}
ostream& operator<<(ostream &os,node*head){
    print(head);
    return os;
}

node* midpoint(node*head){
  node*slow=head;
  node*fast=head;
  while(fast!=NULL and fast->next!=NULL){
    fast=fast->next->next;
    slow=slow->next;
  }
  return slow;
}
node* merge(node*a,node*b){

  if(a==NULL)
    return b;
  if(b==NULL)
    return a;

      node*c;
  if(a->data < b->data){
    c=a;
    c->next=merge(a->next,b);
  }
  else{
    c=b;
    c->next=merge(a,b->next);
  }
  return c;
}
node* merge_sort(node*head){
  //base case
    if(head==NULL or head->next==NULL)
    {
        return head;
    }
    node*mid=midpoint(head);
    node*a=head;
    node*b=mid->next;
    mid->next=NULL;
    a=merge_sort(a);
    b=merge_sort(b);
    node*c=merge(a,b);
    return c;

}


int main(){
    node*head=NULL;
    node*head1=NULL;
    int t;
    cin>>t;

    while(t--){
            int n1,n2;
        cin>>n1>>n2;
        for(int i=0;i<n1;i++){
        int data;
        cin>>data;
        insert(head,data);
    }
     for(int i=0;i<n2;i++){
        int data;
        cin>>data;
        insert(head1,data);
    }
    }

    //head2=midpoint(head);
node*head2=merge(head,head1);
    cout<<head2;


    return 0;
}
