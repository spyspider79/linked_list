#include<bits/stdc++.h>
#include<iostream>
#include<list>
using namespace std;
#define pb push_back

int main(){

   list<pair<int,int> > *l;

   int n;
   cin>>n;
   l=new list<pair<int,int> >[n];
   int e;
   cin>>e;
   for(int i=0;i<e;i++){
    int x,y,wt;
    cin>>x>>y>>wt;
    l[x].pb(make_pair(y,wt));
    l[y].pb(make_pair(x,wt));
   }
   //Print the ll
   for(int i=0;i<n;i++){
        cout<<i<<"->";
    for(auto xp:l[i]){
        cout<<"("<<xp.first<<","<<xp.second<<")";
    }
    cout<<endl;
   }



    return 0;
}
