
#include<bits/stdc++.h>
#include<ostream>
using namespace std;

class node{
public:
    int data;
    node *next;
    node(int d){
        data=d;
        next=NULL;
    }
};
int  length(node*head){
    int cnt=0;
    while(head!=NULL){
        cnt++;
        head=head->next;
    }
    return cnt;
}
void print(node*head){
    while(head!=NULL){
        cout<<head->data<<" ->";
        head=head->next;
    }
    cout<<endl;
}
void insertAtHead(node*&head,int d){
    if(head==NULL){
        head=new node(d);
        return;
    }
    node *n=new node(d);
     n->next=head;
     head=n;
}

node* take_input(){
  int d;
  cin>>d;
  node*head=NULL;
  while(d!=-1){
    insertAtHead(head,d);
    cin>>d;
  }
  return head;
}
ostream& operator<<(ostream &os,node*head){
    print(head);
    return os;
}
istream& operator>>(istream &is,node*&head){

  head=take_input();
   return is;
}

node* midpoint(node*head){
  node*slow=head;
  node*fast=head;
  while(fast!=NULL and fast->next!=NULL){
    fast=fast->next->next;
    slow=slow->next;
  }
  return slow;
}
node* merge(node*a,node*b){

  if(a==NULL)
    return b;
  if(b==NULL)
    return a;

      node*c;
  if(a->data < b->data){
    c=a;
    c->next=merge(a->next,b);
  }
  else{
    c=b;
    c->next=merge(a,b->next);
  }
  return c;
}
node* merge_sort(node*head){
  //base case
    if(head==NULL or head->next==NULL)
    {
        return head;
    }
    node*mid=midpoint(head);
    node*a=head;
    node*b=mid->next;
    mid->next=NULL;
    a=merge_sort(a);
    b=merge_sort(b);
    node*c=merge(a,b);
    return c;

}
void removeCycle(node*head,node*slow){
     node*ptr1=slow;
     node*ptr2=head;
     while(ptr2->next!=ptr1->next){
        ptr1=ptr1->next;
        ptr2=ptr2->next;
     }
     ptr1->next=NULL;

}
bool detectCycle(node*head){
  node*slow=head;
  node*fast=head;
  while(fast!=NULL&&fast->next!=NULL){
    fast=fast->next->next;
    slow=slow->next;
    if(slow==fast){
        removeCycle(head,slow);
        return true;
    }
  }
  return false;
}



int main(){
     node*head;
     cin>>head;
     head->next->next->next->next->next = head->next->next;
    bool ans=detectCycle(head);
    if(ans)
        cout<<"cycle found"<<endl;
    else {
        cout<<"cycle not found"<<endl;
    }
     print(head);


    return 0;
}
