#include<bits/stdc++.h>
#include<ostream>
using namespace std;

class node{
public:
    int data;
    node *next;
    node(int d){
        data=d;
        next=NULL;
    }
};
void print(node*head){
    while(head!=NULL){
        cout<<head->data<<" ";
        head=head->next;
    }
    cout<<endl;
}
void insert(node*&head,int data){
    if(head==NULL){
        head=new node(data);
        return;
    }
    node*tail=head;
    while(tail->next!=NULL){
        tail=tail->next;
    }
    tail->next=new node(data);
    return ;
}

node* midpoint(node*head){
  node*slow=head;
  node*fast=head;
  while(fast!=NULL and fast->next!=NULL){
    fast=fast->next->next;
    slow=slow->next;
  }
  return slow;
}
node* merge(node*a,node*b){

  if(a==NULL)
    return b;
  if(b==NULL)
    return a;

      node*c;
  if(a->data < b->data){
    c=a;
    c->next=merge(a->next,b);
  }
  else{
    c=b;
    c->next=merge(a,b->next);
  }
  return c;
}
int  length(node*head){
    int cnt=0;
    while(head!=NULL){
        cnt++;
        head=head->next;
    }
    return cnt;
}
void append(node*&head,int k){
  int len=length(head);
  int cnt=len-k;
  node*slow=head;
  node*fast=head;
  while(cnt--){
    slow=fast;
    fast=fast->next;
  }
  node*temp=fast;
  while(temp->next!=NULL){
    temp=temp->next;
  }
  temp->next=head;
  head=fast;
  slow->next=NULL;

}

int main(){


    node*head;
    node*head1;
    head=NULL;
    head1=NULL;
    int n,k;
    cin>>n;
    int data;
    for(int i=0;i<n;i++){
        cin>>data;
        insert(head,data);
    }
    cin>>k;
    append(head,k);

    print(head);




    return 0;
}

