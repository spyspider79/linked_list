#include<bits/stdc++.h>
#include<ostream>
using namespace std;

class node{
public:
    int data;
    node *next;
    node(int d){
        data=d;
        next=NULL;
    }
};
void print(node*head){
    while(head!=NULL){
        cout<<head->data<<" ";
        head=head->next;
    }
    cout<<endl;
}
void insert(node*&head,int data){
    if(head==NULL){
        head=new node(data);
        return;
    }
    node*tail=head;
    while(tail->next!=NULL){
        tail=tail->next;
    }
    tail->next=new node(data);
    return ;
}

node* midpoint(node*head){
  node*slow=head;
  node*fast=head;
  while(fast!=NULL and fast->next!=NULL){
    fast=fast->next->next;
    slow=slow->next;
  }
  return slow;
}
node* merge(node*a,node*b){

  if(a==NULL)
    return b;
  if(b==NULL)
    return a;

      node*c;
  if(a->data < b->data){
    c=a;
    c->next=merge(a->next,b);
  }
  else{
    c=b;
    c->next=merge(a,b->next);
  }
  return c;
}



int main(){

    int t;
    cin>>t;
    node*head;
    node*head1;
    while(t--){
        head=NULL;
        head1=NULL;
        int n1,n2;
        cin>>n1;
        int data;
        for(int i=0;i<n1;i++){

        cin>>data;
        insert(head,data);
    }
     print(head);
    cin>>n2;

     for(int i=0;i<n2;i++){
        cin>>data;
        insert(head1,data);
    }
    print(head1);
        //head2=midpoint(head);
      node*h2=merge(head,head1);
      print(h2);
    }



    return 0;
}
