#include<bits/stdc++.h>
#include<ostream>
using namespace std;

class node{
public:
    int data;
    node *next;
    node(int d){
        data=d;
        next=NULL;
    }
};
// Linked class(object oriented)
/*
class LinkedList{
 node*head;
 node*tail;

 public:
     LinkedList(){
     }
     void insert(int d){
     }
};
*/

//Functions(procedural Programming)
void build(){
}
void insertAtHead(node*&head,int d){

    node *n=new node();
    n->data=d;
     n->next=head;
     head=n;
}
//passing by value
void print(node*head){
    while(head!=NULL){
        cout<<head->data<<" ->";
        head=head->next;
    }
    cout<<endl;
}
int  length(node*head){
    int cnt=0;
    while(head!=NULL){
        cnt++;
        head=head->next;
    }
    return cnt;
}
void insertAtTail(node*&head,int data){
    if(head==NULL){
        head=new node(data);
        return;
    }
    node*tail=head;
    while(tail->next!=NULL){
        tail=tail->next;
    }
    tail->next=new node(data);
    return ;
}
void insertInMiddle(node*&head,int d,int p){
  //corner case
  if(head==NULL || p==0){
    insertAtHead(head,d);
  }
  else if(p>length(head)){
    insertAtTail(head,d);
  }
  else {
    int jump=1;
    node*temp=head;
    while(jump<=p-1){
        temp=temp->next;
        jump++;
    }
    node*n=new node(d);
    n->next=temp->next;
    temp->next=n;
  }
}
void deleteHead(node*&head){
   if(head==NULL){
    return;
   }
   node*temp=head;
   head=head->next;
   delete temp;

}
void deleteTail(node*&head){
    node*tail=head;
    node*prev=head;
    while(tail->next!=NULL){
        prev=tail;
        tail=tail->next;
    }
    delete tail;
    prev->next=NULL;


}

//searching Operation
//Linear search
bool search(node*head,int key){

    node*temp=head;
    while(temp!=NULL){
        if(temp->data==key){
            return true;
        }
        temp=temp->next;
    }
    return false;
}
node* take_input(){
  int d;
  cin>>d;
  node*head=NULL;
  while(d!=-1){
    insertAtHead(head,d);
    cin>>d;
  }
  return head;
}
ostream& operator<<(ostream &os,node*head){
    print(head);
    return os;
}
istream& operator>>(istream &is,node*&head){

  head=take_input();
   return is;
}
void reverse(node*&head){
  node*cur=head;
  node*pre=NULL;
  node*n;
  while(cur!=NULL){
      n=cur->next;
      cur->next=pre;
      pre=cur;
      cur=n;
  }
  head=pre;
}
// recursively reverse a linked list
node* recReverse(node*head){
  if(head->next==NULL or head==NULL)
    return head;
  //rec case
   node* small_head=recReverse(head->next);
   node*temp=head->next;
   temp->next=head;
   head->next=NULL;

  return small_head;
}
node* kTheNodefromEnd(node*head,int k){

    node*fast=head;
    node*slow=head;
    while(k>0){
        fast=fast->next;
        k--;

    }
    while(fast!=NULL){
        slow=slow->next;
        fast=fast->next;
    }
 return slow;
}


int main(){
  node*head;
  node*head2;
  cin>>head;
  cout<<head;
  reverse(head);
  cout<<head;
  head2=kTheNodefromEnd(head,3);
  cout<<head2->data;

  /*
  insertInMiddle(head,5,3);
  print(head);
  deleteHead(head);
  print(head);
  deleteTail(head);
  print(head);
  if(search(head,5))
  cout<<"element found"<<endl;
  else {
    cout<<"Not found"<<endl;
  }
  */


  return 0;


}


